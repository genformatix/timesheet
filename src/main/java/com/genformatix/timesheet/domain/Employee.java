package com.genformatix.timesheet.domain;

public interface Employee {

    /**
     * Returns this employee  name.
     * @return actor firstname
     */
    String getFirstName();

    /**
     * Sets the employee firstname.
     * @param firstname firstname of employee
     */
    void setFirstName( String name );

    /**
     * Returns this employee  name.
     * @return actor lastname
     */
    String getLastName();

    /**
     * Sets the employee lastname.
     * @param firstname lastname of employee
     */
    void setLastName( String name );

    /**
     * Returns all projects the employee can bill to.
     * @return all projects
     */
    Iterable<Project> getProjects();

    /**
     * Returns the work units for a specific project
     * @param project the project
     * @return work units
     */
    Iterable<Work> getWork( Project project );
}
