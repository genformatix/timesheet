package com.genformatix.timesheet.domain;

import java.util.Date;

public interface Work {
	
	/**
	 * The date of the work unit
	 * @return date
	 */
	public Date getDate();
	
	/**
	 * The date of the work unit
	 * @param date
	 */
	public void setDate(Date date);
	
	/**
	 * The number of hours worked on the project that day
	 * @return hours
	 */
	public double getHours();
	
	/**
	 * The number of hours worked on the project that day
	 * @param hours
	 */
	public void setHours(double hours);
	
	/**
	 * The employee who billed the time
	 * @return Employee
	 */
	public Employee getEmployee();
	
	/**
	 * The project the hours were billed to
	 * @return Project
	 */
	public Project getProject();

}
