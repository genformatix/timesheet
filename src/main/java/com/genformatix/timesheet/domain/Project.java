package com.genformatix.timesheet.domain;

public interface Project {
	
	/**
	 * Project name
	 * @return name
	 */
	public String getName();
	
	/**
	 * Project name
	 * @param name
	 */
	public void setName(String name);

}
